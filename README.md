# OpenLayersVue 官方示例程序

本项目技术栈 vue3.2 + vite4 + element-plus2.2.32 + openlayers7.2.2

## 目的、内容

1. 使用 vue 技术栈+elementUI 库将 openlayers 官方示例程序重写，使其可以在 vue 环境下丝滑运行

2. 增加原创应用示例

## 演示地址

搭建中，敬请期待...

## 更新日志

## 0.0.1.20230302

- 【项目搭建】初始化项目, 代码整理
- 【官方示例】新增 Accessible Map - 可访问的地图
- 【官方示例】新增 Advanced Mapbox Vector Tiles - 高级 Mapbox 矢量切片地图
- 【官方示例】新增 Advanced View Positioning - 视图定位进阶
- 【官方示例】新增 Animated GIF - GIF 动画
