/**
 * MIT License
 *
 * Copyright (c) 2023 小辉不会飞
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
export const menu = [
  {
    nameEN: "Accessible Map",
    nameCN: "可访问的地图",
    page: "accessibleMap",
    tags: ["osm", "地图缩放", "初始化地图", "易用性"],
    descEN: "Example of an accessible map.",
    descCN: "可访问的地图示例.",
  },
  {
    nameEN: "Advanced Mapbox Vector Tiles",
    nameCN: "高级Mapbox矢量切片地图",
    page: "advancedMapboxVectorTiles",
    tags: ["mapbox", "矢量地图"],
    descEN: "Example of a Mapbox vector tiles map with custom tile grid.",
    descCN: "Mapbox矢量切片地图示例, 支持可自定义切片.",
  },
  {
    nameEN: "Advanced View Positioning",
    nameCN: "视图定位进阶",
    page: "advancedViewPositioning",
    tags: ["osm", "定位", "视图", "进阶"],
    descEN:
      "This example demonstrates how a map's view can be adjusted so a geometry or coordinate is positioned at a specific pixel location.",
    descCN:
      "此示例演示如何调整地图视图, 以便将几何图形或坐标定位到特定像素位置.",
  },
  {
    nameEN: "Animated GIF",
    nameCN: "GIF动画",
    page: "animatedGif",
    tags: ["gif", "动画", "矢量图", "样式", "图标", "暗黑风格地图"],
    descEN: "Example of using an animated GIF as an icon.",
    descCN: "使用GIF动画作为图标的示例.",
  },
];
